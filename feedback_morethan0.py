# create a list that contains all the 5 letters. don't remove any letters. 
from itertools import combinations as nCr
import random

feedback = 3 # here feedback can be anything bw 2 to 5 
word = "gakxz" # sample inputs
guess_word = "mckag"
words = ['mckfg', 'mck', 'vxocd', 'xasbf', 'hxvhf']
def better_feedback(feedback: str, word: str, words: list, guess_word: str) -> str:
    if feedback >= 2:
        getChars = [str(chars) for chars in nCr(guess_word, feedback)]
        chars, i = getChars[0], 1
        while chars not in word:
            chars = getChars[i + 1]
    guess_word = random.choice(words, 'char')
    return guess_word
print(better_feedback(feedback, word, words, guess_word))
