feedback = 0
word = "vxyz"
words = ['abcd', 'abzje', 'peocd', 'xasbf', 'hyvhf']
def better_feedback(feedback: str, word: str, words: list) -> str:
    if feedback == 0:
        for ch in word:
            wrong_words = [wrong_word for wrong_word in words if ch in wrong_word]
            words = list(set(words) - set(wrong_words)) 
    return words

print(better_feedback(feedback, word, words))
