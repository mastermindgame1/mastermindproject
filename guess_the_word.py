import random

class WordGame:
    def __init__(self, filename):
        with open(filename, 'r') as f:
            self.words = [line.strip() for line in f]
        self.word = random.choice(self.words)
        self.guesses = 0

    def play(self):
        while True:
            guess = input("Guess a 5-letter word (or type 'quit' to exit): ").lower()
            if guess == 'quit':
                print(f"You quit the game. The word was: {self.word}")
                break
            if not self.is_valid_guess(guess):
                print("Please enter a 5-letter word.")
                continue
            self.guesses += 1
            common = self.count_common_letters(guess)
            print(f"{common} common letters")
            if guess == self.word:
                print(f"Congratulations! You guessed the word in {self.guesses} guesses.")
                break

    def is_valid_guess(self, guess):
        return len(guess) == 5 and guess.isalpha()

    def count_common_letters(self, guess):
        word_letter_count = {}
        for letter in self.word:
            if letter in word_letter_count:
                word_letter_count[letter] += 1
            else:
                word_letter_count[letter] = 1

        common_count = 0
        for letter in guess:
            if letter in word_letter_count and word_letter_count[letter] > 0:
                common_count += 1
                word_letter_count[letter] -= 1

        return common_count

game = WordGame('words.txt')
game.play()
